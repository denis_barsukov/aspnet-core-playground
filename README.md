# Demo Web API Application 

## Git clone
git clone https://denis_barsukov@bitbucket.org/denis_barsukov/aspnet-core-playground.git

## Steps before run
- install .NET Core SDK 3.1.101 (https://dotnet.microsoft.com/download)
- install vs code (https://visualstudio.microsoft.com/ru/)
- install vs code c# plugin (https://marketplace.visualstudio.com/items?itemName=ms-vscode.csharp)

## How to check that it's works

- run app
- open `https://localhost:5001/weatherforecast`
- view `DEBUG_CONSOLE` log messages

## App configuration

`.vscode/launch.json` - set env vars like:  
```json
	"env": {
		"ASPNETCORE_ENVIRONMENT": "Development",
		"ASPNETCORE_DAYS_COUNT": "10",
		"Logging__LogLevel__Default": "Debug"
	},
```
or edit appropriate configuration file `appsettings.json`, `appsettings.Development.json`, etc..  
or use command line params  

## Links

- https://metanit.com/sharp/aspnet5/2.6.php
- https://docs.microsoft.com/ru-ru/aspnet/core/fundamentals/configuration/?view=aspnetcore-2.2

## YAML config

- https://habr.com/ru/post/453416/
- https://github.com/qdimka/netcore-configuration-sample.git
